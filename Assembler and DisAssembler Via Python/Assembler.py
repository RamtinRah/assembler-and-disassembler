# opcodes
opcodes = {"add": "000000", "adc": "000100", "cmp": "001110", "test": "100001", "xor": "001100",
           "stc": "11111001", "clc": "11111000", "std": "11111101", "cld": "11111100", "syscall": "0000111100000101",
           "ret": "11000011",
           "neg": "111101", "not": "111101", "inc": "111111", "dec": "111111"}
itr = {"add": "100000", "adc": "100000", "cmp": "100000", "test": "111101", "xor": "100000"}
itm = {"add": "100000", "adc": "100000", "cmp": "100000", "test": "111101", "xor": "100000"}
ita = {"add": "0000010", "adc": "0001010", "cmp": "0011110", "test": "1010100", "xor": "0011010"}
regis = {"add": "000", "adc": "010", "cmp": "111", "test": "000", "xor": "110",
         "neg": "011", "not": "010", "inc": "000", "dec": "001", "ret": "11000010"}
# numbers
numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0"]
opsz_dic = {"qword": "64", "dword": "32", "word": "16", "byte": "8"}
# all registers
regs = ["al", "ax", "eax", "rax",
        "cl", "cx", "ecx", "rcx",
        "dl", "dx", "edx", "rdx",
        "bl", "bx", "ebx", "rbx",
        "ah", "sp", "esp", "rsp",
        "ch", "bp", "ebp", "rbp",
        "dh", "si", "esi", "rsi",
        "bh", "di", "edi", "rdi",
        "r8", "r8d", "r8w", "r8b",
        "r9", "r9d", "r9w", "r9b",
        "r10", "r10d", "r10w", "r10b",
        "r11", "r11d", "r11w", "r11b",
        "r12", "r12d", "r12w", "r12b",
        "r13", "r13d", "r13w", "r13b",
        "r14", "r14d", "r14w", "r14b",
        "r15", "r15d", "rw15", "r15b"]
# registers codes
regdic = {"al": "0000", "ax": "0000", "eax": "0000", "rax": "0000",
          "cl": "0001", "cx": "0001", "ecx": "0001", "rcx": "0001",
          "dl": "0010", "dx": "0010", "edx": "0010", "rdx": "0010",
          "bl": "0011", "bx": "0011", "ebx": "0011", "rbx": "0011",
          "ah": "0100", "sp": "0100", "esp": "0100", "rsp": "0100",
          "ch": "0101", "bp": "0101", "ebp": "0101", "rbp": "0101",
          "dh": "0110", "si": "0110", "esi": "0110", "rsi": "0110",
          "bh": "0111", "di": "0111", "edi": "0111", "rdi": "0111",
          "r8": "1000", "r8d": "1000", "r8w": "1000", "r8b": "1000",
          "r9": "1001", "r9d": "1001", "r9w": "1001", "r9b": "1001",
          "r10": "1010", "r10d": "1010", "r10w": "1010", "r10b": "1010",
          "r11": "1011", "r11d": "1011", "r11w": "1011", "r11b": "1011",
          "r12": "1100", "r12d": "1100", "r12w": "1100", "r12b": "1100",
          "r13": "1101", "r13d": "1101", "r13w": "1101", "r13b": "1101",
          "r14": "1110", "r14d": "1110", "r14w": "1110", "r14b": "1110",
          "r15": "1111", "r15d": "1111", "r15w": "1111", "r15b": "1111"}

regszdic = {"al": 8, "ax": 16, "eax": 32, "rax": 64,
            "cl": 8, "cx": 16, "ecx": 32, "rcx": 64,
            "dl": 8, "dx": 16, "edx": 32, "rdx": 64,
            "bl": 8, "bx": 16, "ebx": 32, "rbx": 64,
            "ah": 8, "sp": 16, "esp": 32, "rsp": 64,
            "ch": 8, "bp": 16, "ebp": 32, "rbp": 64,
            "dh": 8, "si": 16, "esi": 32, "rsi": 64,
            "bh": 8, "di": 16, "edi": 32, "rdi": 64,
            "r8": 64, "r8d": 32, "r8w": 16, "r8b": 8,
            "r9": 64, "r9d": 32, "r9w": 16, "r9b": 8,
            "r10": 64, "r10d": 32, "r10w": 16, "r10b": 8,
            "r11": 64, "r11d": 32, "r11w": 16, "r11b": 8,
            "r12": 64, "r12d": 32, "r12w": 16, "r12b": 8,
            "r13": 64, "r13d": 32, "r13w": 16, "r13b": 8,
            "r14": 64, "r14d": 32, "r14w": 16, "r14b": 8,
            "r15": 64, "r15d": 32, "r15w": 16, "r15b": 8}

# ss numbers
sss = {1: "00", 2: "01", 4: "10", 8: "11"}
# hex numbers
hexx = {10: "A", 11: "B", 12: "C", 13: "D", 14: "E", 15: "F"}
# global variables
prefix = ""
registers_ad = []
registers_op = []
adsz = 0
opsz = 0
rex = "0100"
code_w = "0"
rex_w = "0"
r = "0"
x = "0"
b = "0"
opcode = ""
# use d instead of s
d = "0"
mod = ""
reg = ""
rm = ""
program_mod = 0
ss = ""
displacement = ""
disp = 0
ind = ""
bas = ""
bas_bool = False
imm = False
bytes = 0
immidiate = ""
code = ""


# vase avaz kardane mabna
def dec_to_hex(s):
    return str(hex(int(s)))


def bin_to_hex(s):
    # return str(hex(int(s , 2)))[2:]
    code = ""
    for i in range(int(len(s) / 4)):
        x = 0
        for j in range(4):
            x = x * 2 + int(s[4 * i + j])

        if (x < 10):
            code += str(x)
        else:
            code += hexx[x]
    return code


def get_code_w(opsz):
    if opsz == 8:
        return "0"
    return "1"


def get_rex_w(opsz):
    if opsz == 64:
        return "1"
    return "0"


def get_ad_size():
    global registers_ad, program_mod
    if (len(registers_ad) == 0):
        return program_mod
    return regszdic[registers_ad[-1]]


def get_op_size():
    global opsz, registers_op, program_mod
    if (len(registers_op) == 0):
        return program_mod
    return regszdic[registers_op[-1]]


def get_pref():
    global rex_w, opsz, adsz
    p = ""
    if (rex_w == "0" and opsz == 16):
        p += "66"
    if (rex_w == "1" and opsz == 64):
        p += "66"
    if (adsz == 32):
        p += "67"
    return p


def get_program_size():
    global registers_ad, registers_op

    for i in registers_ad:
        if (i[0] == "r"):
            return 64
    for i in registers_op:
        if (i[0] == "r"):
            return 64
    return 32


def make_code():
    global code, rm, ind, reg, bas, b, x, r, displacement, disp, rex_w, code_w, imm, immidiate, ss, bas_bool, opcode, d, mod, opsz, adsz, prefix, program_mod
    program_mod = get_program_size()
    adsz = get_ad_size()
    opsz = get_op_size()
    prefix = get_pref()
    if (len(rm) == 4):
        b = rm[0]
        rm = rm[1:]
    if (len(ind) == 4):
        x = ind[0]
        ind = ind[1:]
    if (len(reg) == 4):
        r = reg[0]
        reg = reg[1:]
    if (len(bas) == 4):
        b = bas[0]
        bas = bas[1:]
    code_w = get_code_w(opsz)
    rex_w = get_rex_w(opsz)
    if (program_mod == 64):
        rex = "0100"
        if (disp > 0 and ind == "100" and bas == "101" and rm == "100" and mod == "00"):
            displacement += (8 - disp) * "0"
            disp = 8

        if (imm):
            displacement = immidiate
        code = rex + rex_w + r + x + b + opcode + d + code_w + mod + reg + rm
        if (bas_bool):
            code += ss + ind + bas
    else:

        code = opcode + d + code_w + mod + reg + rm
        if (bas_bool):
            code += ss + ind + bas
    code = bin_to_hex(code)
    code = prefix + code + displacement


# make memory
def make_memory():
    global rm, mod, bas, b, disp, displacement, ind
    if (bas == "101"):
        mod = "00"
        rm = "100"
        displacement += (8 - disp) * "0"
        disp = 8
    elif (ind == "100" and bas != "0101" and disp == 0):
        mod = "00"
        rm = bas
    elif (ind == "100" and bas != "0101" and disp <= 2):
        mod = "01"
        rm = bas
    elif (ind == "100" and bas != "0101" and disp > 2):
        mod = "10"
        rm = bas
    elif (disp <= 2 and bas == "0101" and ind != "100"):
        rm = "100"
        mod = "01"
        displacement += (2 - disp) * "0"
        disp = 2
    elif (disp >= 3 and bas == "0101" and ind != "100"):
        rm = "100"
        mod = "10"
    elif (disp <= 2 and bas == "0101" and ind == "100"):
        rm = bas
        mod = "01"
        displacement += (2 - disp) * "0"
        disp = 2
    elif (disp >= 3 and bas == "0101" and ind == "100"):
        rm = bas
        mod = "10"
    elif (ind != "100" and disp == 0 and bas != "0101"):
        rm = "100"
        mod = "00"
    elif (disp <= 2 and bas != "0101" and ind != "100"):
        mod = "01"
        rm = "100"
    elif (disp <= 8 and bas != "0101" and ind != "100"):
        mod = "10"
        rm = "100"





# make a hex number to a displacement or immidiate data
def adad_hex(x):
    global disp
    if (x[:2] != "0x"):
        x = dec_to_hex(x)
    disp = max(len(x) - 2, disp)
    if (disp > 2):
        disp = 8
    elif (disp > 0):
        disp = 2
    else:
        disp = 0
    displace = ""
    z = 1
    if (len(x) - 2 > 1):
        for i in range(disp):
            if (i < len(x) - 2):
                displace += x[len(x) - i - 1 - z]
                z = -z
            else:
                displace += "0"
    else:
        displace = "0" + x[-1]
    return displace


# immidiate data
def immidiate_hex(x):
    global bytes
    if (x[:2] != "0x"):
        x = dec_to_hex(x)
    disp = len(x) - 2
    if (disp > 4):
        disp = 8
    elif (disp > 2):
        disp = 4
    else:
        disp = 2
    bytes = disp
    displace = ""
    z = 1
    if (len(x) - 2 > 1):
        for i in range(disp):
            if (len(x) - i - 1 - z >= 2):
                displace += x[len(x) - i - 1 - z]
            else:
                displace += "0"
            z = -z
    else:
        displace = "0" + x[-1]
    return displace


# memory to code(if its memory and return 1 else return 0)
def memory(s):
    global opsz, bas_bool, displacement, ss, adsz, ind, bas, registers_op, registers_ad, disp
    # size of data in address
    if (s[:5] in opsz_dic):
        opsz = int(opsz_dic[s[:5]])
        s = s[5:]

    # adress
    if (s[0] == "[" and s[-1] == "]"):
        bas_bool = True
        disp = 0
        displacement = ""
        s = s[1:len(s) - 1]
        s = s.split("+")
        b = False
        if (s[len(s) - 1][:2] == "0x" or s[len(s) - 1][0] in numbers):
            b = True
            displacement = adad_hex(s[len(s) - 1])
        if (len(s) == 1 and b):
            adsz = 64
            ss = sss[1]
            ind = "100"
            bas = "101"
        elif (len(s) == 1 and not b):
            x = s[0].split("*")
            registers_ad.append(x[0])
            if (len(x) == 1):
                ss = sss[1]
                ind = "100"
                bas = regdic[s[0]]
                if (regdic[x[0]] != "0100"):
                    bas_bool = False
            else:
                ss = sss[int(x[1])]
                ind = regdic[x[0]]
                bas = "101"
        elif (len(s) == 2 and b):
            x = s[0].split("*")
            registers_ad.append(s[0])
            if (len(x) == 1):
                if (regdic[x[0]] != "0100"):
                    bas_bool = False
                ss = sss[1]
                ind = "100"
                bas = regdic[s[0]]
            else:
                ss = sss[int(x[1])]
                ind = regdic[x[0]]
                bas = "101"
        else:
            x = s[0].split("*")
            y = s[1].split("*")
            registers_ad.append(x[0])
            registers_ad.append(y[0])
            if (len(x) == 1 and len(y) == 1):
                if (regdic[y[0]] == "0100"):
                    bas = regdic[y[0]]
                    ind = regdic[x[0]]
                    ss = sss[1]
                else:
                    bas = regdic[x[0]]
                    ind = regdic[y[0]]
                    ss = sss[1]
            else:
                if (len(y) == 1):
                    ss = sss[1]
                else:
                    ss = sss[int(y[1])]
                ind = regdic[y[0]]
                bas = regdic[x[0]]
        return 1
    else:
        return 0


def tof(s):
    if len(s) == 1:
        return s
    s[1] = s[1].split(",")
    _s = []
    _s.append(s[0])
    _s.extend(s[1])
    return _s


# input is like this ( add rax,rbx )

s = list(input().split())
s = tof(s)

# model haye mokhtalef input baraye tavabe ba 3 vorodi
if (len(s) == 3):
    opcode = opcodes[s[0]]
    if (s[1] in regs and s[2] in regs):
        registers_op.append(s[1])
        registers_op.append(s[2])
        d = "0"
        mod = "11"
        reg = regdic[s[2]]
        rm = regdic[s[1]]
    elif (memory(s[1]) and s[2] in regs):
        d = "0"
        registers_op.append(s[2])
        make_memory()
        reg = regdic[s[2]]
    elif (s[1] in regs and memory(s[2])):
        d = "1"
        registers_op.append(s[1])
        make_memory()
        reg = regdic[s[1]]
    else:
        imm = True
        d = "1"
        immidiate = immidiate_hex(s[2])
        if (s[1] in regs):
            registers_op.append(s[1])
            if (regdic[s[1]] == "1000"):
                opcode = ita[s[0]]
                reg = ""
                rm = ""
                mod = ""
                d = ""
            else:
                opcode = itr[s[0]]
                if (bytes * 4 != 16 and bytes * 4 != 32):
                    d = "1"
                else:
                    d = "0"
                mod = "11"
                reg = regis[s[0]]
                rm = regdic[s[1]]
        else:
            opcode = itm[s[0]]
            if (bytes * 4 != 16 and bytes * 4 != 32):
                d = "1"
            else:
                d = "0"
            reg = regis[s[0]]
            memory(s[1])
            make_memory()
    make_code()
elif (len(s) == 1):
    code = opcodes[s[0]]
    code = bin_to_hex(code)
elif (len(s) == 2):
    d = "1"
    opcode = opcodes[s[0]]
    if (s[1] in regs):
        registers_op.append(s[1])
        mod = "11"
        rm = regdic[s[1]]
    elif (memory(s[1])):
        make_memory()
    reg = regis[s[0]]
    make_code()

print(code)
