# opcodes
opcodes = {"0000111110111100": "bsf", "0000111110111101": "bsr", "00001111110000": "xadd", "100001": "xchg",

           "111101": "not", "111101": "neg", "111111": "inc", "111111": "dec"}
itr = ["not", "neg", "inc", "dec"]

opcodes0 = {"F9": "stc", "F8": "clc", "FD": "std", "FC": "cld", "0F05": "syscall",
            "C3": "ret", }

# 8bits registers
reg8 = ["al", "cl", "dl", "bl", "ah", "ch", "dh", "bh", "r8b", "r9b", "r10b", "r11b", "r12b", "r13b", "r14b", "r15b"]
reg_8 = {"0000": "al", "0001": "cl", "0010": "dl", "0011": "bl", "0100": "ah", "0101": "ch", "0110": "dh", "0111": "bh"
    , "1000": "r8b", "1001": "r9b", "1010": "r10b", "1011": "r11b", "1100": "r12b", "1101": "r13b", "1110": "r14b",
         "1111": "r15b"}
# 16bits registers
reg16 = ["ax", "cx", "dx", "bx", "sp", "bp", "si", "di", "r8d", "r9d", "r10w", "r11w", "r12w", "r13w", "r14w", "rw15"]
reg_16 = {"0000": "ax", "0001": "cx", "0010": "dx", "0011": "bx", "0100": "sp", "0101": "bp", "0110": "si", "0111": "di"
    , "1000": "r8w", "1001": "r9w", "1010": "r10w", "1011": "r11w", "1100": "r12w", "1101": "r13w", "1110": "r14w",
          "1111": "r15w"}
# 32bits registers
reg32 = ["eax", "ecx", "edx", "ebx", "esp", "ebp", "esi", "edi", "r8d", "r9d", "r10d", "r11d", "r12d", "r13d", "r14d",
         "r15d"]
reg_32 = {"0000": "eax", "0001": "ecx", "0010": "edx", "0011": "ebx", "0100": "esp", "0101": "ebp", "0110": "esi",
          "0111": "edi"
    , "1000": "r8", "1001": "r9", "1010": "r10b", "1011": "r11b", "1100": "r12b", "1101": "r13b", "1110": "r14b",
          "1111": "r15b"}

reg64 = ["rax", "rcx", "rdx", "rbx", "rsp", "rbp", "rsi", "rdi", "r8", "r9", "r10", "r11", "r12", "r13", "r14", "r15"]
reg_64 = {"0000": "rax", "0001": "rcx", "0010": "rdx", "0011": "rbx", "0100": "rsp", "0101": "rbp", "0110": "rsi",
          "0111": "rdi"
    , "1000": "r8", "1001": "r9", "1010": "r10", "1011": "r11", "1100": "r12", "1101": "r13", "1110": "r14",
          "1111": "r15"}

sss = {"00": "1", "01": "2", "10": "4", "11": "8"}
hex_numbers = {"A": 10, "B": 11, "C": 12, "D": 13, "E": 14, "F": 15}

# global variables
r_w = ""
r_r = "0"
r_x = "0"
r_b = "0"
ss = ""
inx = ""
bas = ""
disp = ""
w = ""
d = ""
p1 = ""
p2 = ""
p3 = ""
prefix1 = ""

prefix2 = ""
p1_out = ""
p2_out = ""
p3_out = ""
word = ""
memory = False


def hex_to_bin(s):
    global hex_numbers
    _s = ""
    if (s >= "0" and s <= "9"):
        x = int(s)
    else:
        x = hex_numbers[s]
    for i in range(4):
        _s = str(x % 2) + _s
        x = int(x / 2)
    return _s


code = input()

if code.upper() in opcodes0.keys():
    print(opcodes0.get(code))
    exit()

if code[:2] == "67":
    prefix1 = "67"
    code = code[2:]

if code[:2] == "66":
    prefix2 = "66"
    code = code[2:]

if code[0] == "4":
    _s = hex_to_bin(code[1])
    r_w = _s[0]
    r_r = _s[1]
    r_x = _s[2]
    r_b = _s[3]
    code = code[2:]

tof = ""

while code[0] == "0":
    tof = tof + "0" * 4
    code = code[1:]

code = tof + bin(int(code, 16))[2:]

if len(code) == 8:
    p1 = r_r + code[5:]
    p2 = r_r + "000"
    w = "1"

    if prefix2 == "66":
        p1_out = reg_16[p1]
        p2_out = reg_16[p2]
    elif r_w == "1":
        p1_out = reg_64[p1]
        p2_out = reg_64[p2]
    elif w == "1":
        p1_out = reg_32[p1]
        p2_out = reg_32[p2]
    else:
        p1_out = reg_8[p1]
        p2_out = reg_8[p2]

    print("xchg " + p1_out + " " + p2_out)
    exit()

for i in range(1, len(code) + 1):
    if code[: i] in opcodes.keys():
        op = code[:i]
        code = code[i:]
        break

opcode = opcodes[op]

if len(op) % 4 != 0:

    d = code[0]
    w = code[1]
    code = code[2:]

else:
    w = "1"

mod = code[:2]
reg = code[2:5]
r_m = code[5:8]
code = code[8:]

if r_m == "100":
    # sib
    ss = code[:2]
    inx = code[2:5]
    bas = code[5:8]
    code = code[8:]

try:

    n = len(code)
    for i in range(0, n, 8):
        disp = code[i:i + 8] + disp

    disp = hex(int(disp, 2))
    disp = "+" + disp



except:
    pass

if r_m == "100":

    p1 = r_r + reg
    p2 = r_b + bas
    p3 = r_x + inx
    if prefix2 == "66":
        p1_out = reg_16[p1]
        word = "WORD"
    elif r_w == "1":
        p1_out = reg_64[p1]
        word = "QWORD"

    elif w == "1":
        p1_out = reg_32[p1]
        word = "DWORD"
    else:
        p1_out = reg_8[p1]
        word = "BYTE"

    if prefix1 == "67":

        p2_out = reg_32[p2]
        p3_out = reg_32[p3]
    else:

        p2_out = reg_64[p2]
        p3_out = reg_64[p3]

    if p3_out == None:
        p3_out = ""
    p2_out = p2_out + "+"
    p3_out = p3_out + "*" + sss.get(ss)

    if mod == "00" and bas == "101":
        p2_out = ""

    if mod == "00" and bas == "101" and inx == "100":
        p2_out = ""
        p3_out = ""
        disp = disp[1:]

    memory = True

if mod == "11":

    p1 = r_r + reg
    p2 = r_b + r_m
    if prefix2 == "66":
        p1_out = reg_16[p1]
        p2_out = reg_16[p2]
    elif r_w == "1":
        p1_out = reg_64[p1]
        p2_out = reg_64[p2]
    elif w == "1":
        p1_out = reg_32[p1]
        p2_out = reg_32[p2]
    else:
        p1_out = reg_8[p1]
        p2_out = reg_8[p2]

elif r_m != "100":
    p1 = r_r + reg
    p2 = r_b + r_m
    if prefix2 == "66":
        p1_out = reg_16[p1]
        word = "WORD"
    elif r_w == "1":
        p1_out = reg_64[p1]
        word = "QWORD"

    elif w == "1":
        p1_out = reg_32[p1]
        word = "DWORD"
    else:
        p1_out = reg_8[p1]
        word = "BYTE"

    if prefix1 == "67":

        p2_out = reg_32[p2]
        p3_out = reg_32[p3]
    else:

        p2_out = reg_64[p2]
        p3_out = reg_64[p3]

    if p3_out == None:
        p3_out = ""
    p2_out = p2_out + "+"
    p3_out = p3_out + "*" + sss.get(ss)

    if mod == "00" and bas == "101":
        p2_out = ""

    if mod == "00" and bas == "101" and inx == "100":
        p2_out = ""
        p3_out = ""
        disp = disp[1:]
    p2_out = p2_out
    memory = True

if opcode in itr:
    p1_out = ""

    if reg[-1] == "0":
        opcode = itr[itr.index(opcode) - 1]

if opcode == "xadd" or opcode == "xchg":
    if memory:
        print(opcode + " " + word + " PTR" + " [" + p2_out + p3_out + disp + "]" + " " + p1_out)
    else:
        print(opcode + " " + p2_out + " " + p1_out + disp)

else:
    if memory:
        print(opcode + " " + p1_out + " " + word + " PTR" + " [" + p2_out + p3_out + disp + "]")
    else:
        print(opcode + " " + p1_out + " " + p2_out + disp)

print(code)
print(bas)
